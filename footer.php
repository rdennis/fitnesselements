<div class="footer">
    <footer>
        <div>
            <p class="copyright">
                Copyright 2009-<?php
                    date_default_timezone_set('America/New_York');
                    echo strftime('%Y');
                ?> Fitness Elements
            </p>
            <p class="credit">
                Website design by Robert Dennis
            </p>
        </div>
    </footer>
</div>